#ifndef RTSP_REQUEST_REPONSE_H
#define RTSP_REQUEST_REPONSE_H
#include "rtsp_client.h"



typedef enum _HdyRtspMethod
{
    RTSP_OPTIONS = 1,
    RTSP_DESCRIBE,
    RTSP_SETUP,
    RTSP_PLAY,
    RTSP_TERARDOWN,
    RTSP_INVALID_METHOD,
}RtspMethod;


RtspMethod get_rtsp_method(char* buffer);

int rtsp_options(struct RtspClient* rtsp_client);
int rtsp_describe(struct RtspClient* rtsp_client);
int rtsp_setup(struct RtspClient* rtsp_client);
int rtsp_play(struct RtspClient* rtsp_client);
int rtsp_terardown(struct RtspClient* rtsp_client);

int rtsp_send_reply(struct RtspClient* thiz, int errornumber);


#endif /*RTSP_REQUEST_REPONSE_H*/
